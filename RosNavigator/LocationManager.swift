//
//  File.swift
//  RosNavigator
//
//  Created by tomaszpaluch on 10/08/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: CLLocationManager {
    var errorCompletion: ((Result<Int, LocationError>) -> Void)!
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationPermission()
        } else {
            errorCompletion(.failure(.locationServiceDisabledError))
        }
    }
    
    private func setupLocationManager() {
        self.desiredAccuracy = kCLLocationAccuracyBest
        self.allowsBackgroundLocationUpdates = true
    }
    
    private func checkLocationPermission() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            errorCompletion(.success(0))
        case .authorizedAlways:
            errorCompletion(.success(0))
        case .denied:
            errorCompletion(.failure(.locationServiceNotAuthorizedError))
        case .notDetermined:
            self.requestWhenInUseAuthorization()
        case .restricted:
            errorCompletion(.failure(.locationServiceRestrictedError))
        @unknown default:
            break
        }
    }
}
