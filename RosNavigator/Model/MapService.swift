//
//  MapService.swift
//  RosNavigator
//
//  Created by tomaszpaluch on 10/08/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation
import GoogleMaps

class MapService {
    private let key = "AIzaSyCBe2m3rpdzYNUeESHTxWCYTt4yd0rcAgo"
    
    private let mutablePath: GMSMutablePath
    private let polyline: GMSPolyline
    
    private var wasLastTimeStampNew: Bool
    
    init() {
        GMSServices.provideAPIKey(key)

        mutablePath = GMSMutablePath()
        polyline = GMSPolyline()
        
        wasLastTimeStampNew = true
    }
    
    func addLocationToPath(_ location: CLLocation) {
        let isActualTimeStampNew = isTimeStampNew(timestamp: location.timestamp)
        
        if isActualTimeStampNew && wasLastTimeStampNew {
            let coordinates = location.coordinate
            mutablePath.add(coordinates)
        }
        
        wasLastTimeStampNew = isActualTimeStampNew
    }
    
    private func isTimeStampNew(timestamp: Date) -> Bool {
        if -timestamp.timeIntervalSinceNow <= 1 {
            return true
        } else {
            return false
        }
    }
    
    func resetService() {
        mutablePath.removeAllCoordinates()
    }
    
    func setMap(map: GMSMapView) {
        polyline.path = mutablePath
        polyline.map = map
    }
}
