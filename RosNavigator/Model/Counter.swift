//
//  Counter.swift
//  RosNavigator
//
//  Created by tomaszpaluch on 10/08/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

class Counter {
    var completion: ((String) -> Void)!
    
    private var timer: Timer!
    private var seconds: Int
    
    init() {
        seconds = 0
    }
    
    func startCounter() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    func resetCounter() {
        timer.invalidate()
        seconds = 0
    }
    
    @objc private func updateCounter() {
        addSec()
        
        let counterText = getCounterAsString()
        completion(counterText)
    }
    
    func addSec() {
        seconds += 1
    }
    
    func getCounterAsString() -> String {
        let (hours, minutes, seconds) = calculateHoursMinutesSeconds()
        
        let hoursString = String(format: "%02d", hours)
        let minutesString = String(format: "%02d", minutes)
        let secondsString = String(format: "%02d", seconds)
        
        return "\(hoursString):\(minutesString):\(secondsString)"
    }
    
    private func calculateHoursMinutesSeconds() -> (Int, Int, Int) {
        var restOfTime = seconds
        
        let hours = restOfTime / 3600
        restOfTime = restOfTime - hours * 3600
        
        let minutes = restOfTime / 60
        restOfTime = restOfTime - minutes * 60
        
        let seconds = restOfTime
        
        return (hours, minutes, seconds)
    }
}
