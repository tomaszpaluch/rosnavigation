//
//  File.swift
//  RosNavigator
//
//  Created by tomaszpaluch on 17/08/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

enum LocationError: Error {
    case locationServiceDisabledError
    case locationServiceNotAuthorizedError
    case locationServiceRestrictedError
}
