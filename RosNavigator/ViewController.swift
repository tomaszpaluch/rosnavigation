//
//  ViewController.swift
//  RosNavigator
//
//  Created by tomaszpaluch on 08/08/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    
    private let locationManager: LocationManager
    private let counter: Counter
    private let mapService: MapService

    required init?(coder aDecoder: NSCoder) {
        locationManager = LocationManager()
        counter = Counter()
        mapService = MapService()
        
        super.init(coder: aDecoder)
        
        counter.completion = { [unowned self] (labelText: String) in
            self.counterLabel.text = labelText
        }
        
        locationManager.errorCompletion = { [unowned self] result in
            switch result {
            case .failure(.locationServiceDisabledError):
                self.showLocationServiceDisabledError()
            case .failure(.locationServiceNotAuthorizedError):
                self.showLocationServiceNotAuthorizedError()
            case .failure(.locationServiceRestrictedError):
                self.showLocationServiceRestrictedError()
            case .success(_):
                self.authorizationSucceded()
            }
        }
        
        locationManager.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        counterLabel.text = ""
        startStopButton.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!

        mapService.addLocationToPath(location)
        mapService.setMap(map: mapView)
        
        mapView.animate(to: GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0))
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.checkLocationServices()
    }

    @IBAction func startStopAction(_ sender: Any) {
        if startStopButton.tag == 0 {
            startNavigation()
        } else {
            resetNavigation()
        }
    }
    
    private func startNavigation() {
        mapService.setMap(map: mapView)
        
        startStopButton.tag = 1
        startStopButton.setTitle("STOP", for: .normal)
        
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        
        counter.startCounter()
    }
    
    private func resetNavigation() {
        startStopButton.tag = 0
        startStopButton.setTitle("START", for: .normal)
        
        mapView.isMyLocationEnabled = false
        locationManager.stopUpdatingLocation()
        
        mapService.resetService()
        counter.resetCounter()
    }
    
    private func authorizationSucceded() {
        startStopButton.isHidden = false
    }
    
    private func showLocationServiceDisabledError() {
        showErrorAlert(message: "Location service is disabled. Please enable it.")
    }
    
    private func showLocationServiceNotAuthorizedError() {
        showErrorAlert(message: "Location service is not authorized. Open Settings -> Privacy -> Location Services to make authorization.")
    }
    
    private func showLocationServiceRestrictedError() {
        showErrorAlert(message: "Your access to the location service is restricted.")
    }
    
    private func showErrorAlert(message: String) {
        let alert = UIAlertController(title: "Location Service Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        startStopButton.isHidden = true
    }
}

