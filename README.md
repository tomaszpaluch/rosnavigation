#  RosNavigation
## requirements
In order to compile and run this app you need to have a Mac running Mac OS Mojave with Xcode 10.2.

## how to run
To run this app in iOS Simulator follow steps:

1. select Source Control -> Clone,
2. enter reposotory url,
3. click clone,
4. choose project name and directory,
5. click clone,
6. click "Build and then run the current scheme" button,

## usage
Tap "Start" button to start navigation and time counter. Application's main view will be centered on actual GPS position, moving will will cause the map to move and the trail will be marked. Tap "Stop" to cease navigation and time counter. Tapping "Start" button again will reset time counter and remove navigational trail.

## architecture pattern
* MVC - Model View Controller, standard pattern for creating apps for iOS, helps to organize the project, keep it in order and make its extension easier. 

## author
Tomasz Paluch - paluch.t@wp.pl - for recruitment process at Ros Media 



